--- Introducción ---

La práctica se ha elaborado pensando en una necesidad relacionada al mundo del
fútbol, actualmente, en el fútbol, la labor de los directores deportivos es tratar durante
todo el año de buscar a grandes jugadores, el problema es que actualmente los
grandes jugadores ganan una fortuna y están al alcance de muy pocos equipos.
A lo largo de los años la labor de un director deportivo siempre ha sido de estar
rodeado de ojeadores, y éstos facilitaban informes de los jugadores en el que
observando los partidos de fútbol, recopilaban toda la información que pudiera ser de
interés para el club, en él se comentaban los pros y contras, siempre desde la ética del
ojeador.

Actualmente la tecnología también ha avanzado y podemos estar sin salir de casa
viendo infinidad de partidos de fútbol durante todo el fin de semana, analizando en
detalle las jugadas, además de disponer herramientas necesarias para obtener
estadísticas exhaustivas de todos los jugadores durante el partido en vivo, de la
temporada o incluso de años anteriores.

Estos sistemas de información pueden ayudar a cubrir una necesidad, un puesto en el
esquema del equipo, abriendo así una ventana a un mundo de estadísticas para poder
fichar a las grandes promesas del fútbol a un precio razonable.

--- Fuente / Datos ---

En la práctica vamos a tratar de analizar el bloque estadístico que se ha recopilado
durante la temporada 17/18 de la Primera División hasta Enero, para ello he definido
personalmente 3 ficheros csv (jugador, estadística y procedencia).

- jugador.csv: contiene la siguiente información:
    * idjug|nombre|posicion|fechaNac|edad|nacionalidad|altura|pie|ffincontraro|valor

- estadistica.csv: contiene el detalle estadistico de cada jugador:
    * idjug|partidosJugados|minutosJugados|golesTotal|asistTotales|accDefOk90|duelosDef90|porcDuelosDefGan|duelosAereosGan90|porcDuelosAereosGan|entradas90|porcEntradasOk|robos90|faltas90|tarjAmarillasTotal|tarjAmarillas90|tarjRojasTotal|tarjRojas90|accOfensivasOk|goles90|golesCabezaTotal|golesCabeza90|tirosTotales|tiros90|porcTirosPorteria|porcGolesOk|asistenciaGol90|centros90|porcCentrosOk|regates90|porcRegatesOk|toquesAreaCont90|pases90|porcPasesOk|pasesAdelante90|porcPasesAdelanteOk|pasesAtras90|porcPasesAtrasOk|pasesLaterales90|porcPasesLateralOk|longMediaPasesMet|desmarques90|porcDesmarquesOk|pases34Contrario90|porcPas34Contrario|pasesLargos90|porcPasesLargosOk|pasesProfund90|porcPasesProfOk|jugClavLonMedPasMet|tirosLibres90|tirosLibresDirect90|porcTirosLibresPort|corners90|penaltysAFavor|porcPenalRealizados

- procedencia.csv:
    * jugador|temporada|fechafichaje|equipoOrigen|paisOrigen|equipoDestino|paisDestino|valor|coste

--- Proyecto: ¿Qué necesito de estos datos? ---

Intentando simular la situación vivida por el Real Betis Balompié a principios del 2018, en el que su central por excelencia: Zou Feddal, en el que desafortunadamente se lesionó del tendón de aquiles, tuve la idea de indagar y buscar jugadores con un perfil específico con similares características que pudiera encajar en el esquema del equipo.

Para ello me planteé que el Real Betis Balompié, para mantener la buena línea del equipo y poder aspirar a jugar competiciones europeas, su director deportivo, solicitaba un informe de los 15 mejores jugadores que tengan el siguiente perfil en concreto:
- Que sean defensa centrales,
- Con una edad inferior a los 28 años,
- Con un buen coeficiente en duelos defensivos. 
- Que tenga un buen coeficiente en remates de cabeza a portería.
- Que tenga buena valoración filtrando pases hacia delante.
- Con un valor de mercado que no supere los 4500000 de €.
- Y cuya fecha fin de contrato no sea posterior al 12/31/2021 (formato mes/dia/año)

--- Solución funcional ---

Para obtener un resultado más óptimo realizaré la misma consulta en 3 partes, en función de:
1. coeficiente en duelos defensivos --> campos ordenados según porcDuelosDefGan (porciento de duelos defensivos ganados) y duelosDef90 (media de duelos defensivos por partido).
2. coeficiente en remates de cabeza a portería --> campos ordenados según golesCabeza90 (media de goles de cabeza por partido) y golesCabezaTotal (goles de cabeza en total).
3. buena valoración filtrando pases hacia adelante --> campos ordenados según porcPasesAdelanteOk (porciento de pases hacia delante logrados) y pasesAdelante90 (media de pases hacia delante por partido).

Y de cada punto recuperaré los 5 mejores en cada aspecto (hasta completar los 15 que se ha solicitado).


